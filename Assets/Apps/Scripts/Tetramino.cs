using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Tetramino : MonoBehaviour, ITetramino
{
    [SerializeField] private Transform pivot;
    [ShowInInspector] private List<IBlock> blocks = new();

    private void Awake()
    {
        blocks.AddRange(GetComponentsInChildren<IBlock>());
    }

    public bool Move(int x, int y)
    {
        if (!IsValidMove(x, y))
        {
            return false;
        }
        transform.Translate(x, y, 0);
        return true;
    }

    public bool IsValidMove(int x, int y)
    {


        return true;
    }

    public bool Rotate(RotateDirection rotateDirection)
    {
        transform.RotateAround(pivot.position, Vector3.back, ((float)rotateDirection));
        return true;
    }
}
