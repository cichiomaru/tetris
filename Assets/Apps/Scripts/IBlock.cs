public interface IBlock
{
    public void Separate();
    public bool IsValidLocation(int x, int y);
}
