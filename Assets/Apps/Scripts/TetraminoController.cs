using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetraminoController : MonoBehaviour
{
    [ShowInInspector] private ITetramino activeTetramino;
    [SerializeField] private List<GameObject> tetraminos;

    private void Start()
    {
        Spawn();

        StartCoroutine(MoveDown());
    }

    private void Update()
    {
        if (activeTetramino is null)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            activeTetramino.Move (-1, 0);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            activeTetramino.Move (1, 0);
        }
    }

    private void Spawn()
    {
        GameObject go = Instantiate(tetraminos[0], transform.position, transform.rotation);
        activeTetramino = go.GetComponent<ITetramino>();
    }

    private IEnumerator MoveDown()
    {
        while (true)
        {
            activeTetramino.Move(0, -1);

            yield return new WaitForSeconds(1);
        }
    }
}
