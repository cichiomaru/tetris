﻿public interface ITetramino
{
    public bool Move(int x, int y);
    public bool Rotate(RotateDirection rotateDirection);
    public bool IsValidMove(int x, int y);
}